import {Component, EventEmitter, HostListener, Input, OnChanges, OnInit, Output} from '@angular/core';

@Component({
  selector: 'bd-bread-crumbs',
  templateUrl: './bread-crumbs.component.html',
  styleUrls: ['./bread-crumbs.component.scss']
})
export class BreadCrumbsComponent implements OnChanges, OnInit {

  @Input() states: any[];
  @Input() currentState: any;

  currentStateIndex: number;

  screenWidth: number = window.innerWidth
    || document.documentElement.clientWidth
    || document.body.clientWidth;
  splitPoint: number = ((this.screenWidth - 330) - (this.screenWidth - 330) % 150) / 150 - 1;
  isOpen = false;

  @Output() stateChanged: EventEmitter<any> = new EventEmitter();

  constructor() {
  }

  get getScreenWidth(): number {
    return window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth;
  }

  get getSplintPoint(): number {
    return ((this.screenWidth - 330) - (this.screenWidth - 330) % 150) / 150 - 1;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.screenWidth = this.getScreenWidth;
    this.splitPoint = this.getSplintPoint;
  }

  ngOnChanges() {
    this.currentStateIndex = this.states.findIndex(state => state === this.currentState);
  }

  ngOnInit() {
    this.screenWidth = this.getScreenWidth;
    this.splitPoint = this.getSplintPoint;
  }

  onStateClicked(state: any) {
    if (state !== this.currentState) {
      this.stateChanged.emit(state);
    }
  }

  closeMenu() {
    this.isOpen = false;
  }

}
