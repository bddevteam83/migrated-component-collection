import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BreadCrumbsComponent} from './bread-crumbs.component';
import {MatIconModule, MatMenuModule, MatTooltipModule} from '@angular/material';
import {SliceWithDotsPipeModule} from 'bd-pipe-collection';

@NgModule({
  declarations: [BreadCrumbsComponent],
  imports: [
    CommonModule,

    MatTooltipModule,
    MatMenuModule,
    MatIconModule,

    SliceWithDotsPipeModule,
  ],
  exports: [BreadCrumbsComponent]
})
export class BreadCrumbsComponentModule {
}
