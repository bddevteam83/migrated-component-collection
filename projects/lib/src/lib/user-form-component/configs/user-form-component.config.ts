import {GroupModel} from '../models/group.model';
import {UserModel} from '../models/user.model';

export interface UserFormComponentConfig {
  user: UserModel;
  config: {
    title: string;
    translatePrefix: string;
    actions: {
      close: string,
      submit: string
    };
    languages: string[];
    groups: GroupModel[];
  };
}
