import {AuthorityModel} from './authority.model';

export interface GroupModel {
  id?: number;
  name?: string;
  groupOfGroup?: string;
  authorities?: AuthorityModel[];
}
