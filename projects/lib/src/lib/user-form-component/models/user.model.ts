import {GroupModel} from './group.model';

export interface UserModel {
  id?: number;
  login?: string;
  firstName?: string;
  lastName?: string;
  langKey?: string;
  email?: string;
  activated?: boolean;
  groups?: GroupModel[];
}
