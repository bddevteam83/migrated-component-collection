import {RoleModel} from './role.model';

export interface SubTypeModel {
  subType?: string;
  roles?: RoleModel[];
}
