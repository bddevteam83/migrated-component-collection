import {SubTypeModel} from './sub-type.model';

export interface AuthorityModel {
  type?: string;
  subTypes?: SubTypeModel[];
}
