import {Component, ElementRef, Inject, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatAutocomplete, MatAutocompleteSelectedEvent} from '@angular/material';
import {UserModel} from './models/user.model';
import {FormArray, FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {UserFormComponentConfig} from './configs/user-form-component.config';
import {GroupModel} from './models/group.model';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {AuthorityModel} from './models/authority.model';
import {SubTypeModel} from './models/sub-type.model';
import {RoleModel} from './models/role.model';

@Component({
  selector: 'bd-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent {
  /**
  *  Some
  * Comment
  */
  formGroup: FormGroup;

  groupsCtrl: FormControl = new FormControl();
  filteredGroups$: Observable<GroupModel[]>;

  @ViewChild('groupsInput') groupsInput: ElementRef<HTMLInputElement>;
  @ViewChild('groupsAutocomplete') matAutocomplete: MatAutocomplete;

  constructor(@Inject(MAT_DIALOG_DATA) public data: UserFormComponentConfig,
              private fb: FormBuilder) {

    this.formGroup = this._userForm(this.data.user);

    this.filteredGroups$ = this.groupsCtrl.valueChanges
      .pipe(startWith(null),
        map((value: GroupModel | string) => {
          switch (true) {
            case (typeof value === 'string'):
              return this._filter(<string>value);
            case (value instanceof Object):
            default:
              return this._uniqGroups();
          }
        }));
  }

  inputGroup(): void {
    if (!this.matAutocomplete.isOpen) {
      this.groupsInput.nativeElement.value = '';
      this.groupsCtrl.setValue(null);
    }
  }

  removeGroup(group: GroupModel): void {
    const index = (<GroupModel[]>this.formGroup.get('groups').value).indexOf(group);

    if (index >= 0) {
      (<FormArray>this.formGroup.get('groups')).removeAt(index);
    }

    this.groupsInput.nativeElement.value = '';
    this.groupsCtrl.setValue(null);
  }

  selectGroup($event: MatAutocompleteSelectedEvent): void {
    (<FormArray>this.formGroup.get('groups')).push(this._groupForm($event.option.value));

    this.groupsInput.nativeElement.value = '';
    this.groupsCtrl.setValue(null);
  }

  private _filter(value: string): GroupModel[] {
    return this._uniqGroups()
      .filter(group => group.name.toLowerCase().indexOf(value.toLowerCase()) === 0);
  }

  private _uniqGroups(): GroupModel[] {
    return this.data.config.groups
      .filter(group => ((<GroupModel[]>this.formGroup.get('groups').value)
        .findIndex(currentGroup => currentGroup.id === group.id)) === -1);
  }

  private _userForm(obj: UserModel = {}): FormGroup {
    return this.fb.group({
      id: obj.id || null,
      login: obj.login || null,
      firstName: obj.firstName || null,
      lastName: obj.lastName || null,
      langKey: obj.langKey || null,
      email: obj.email || null,
      activated: obj.activated || null,
      groups: this.fb.array(obj.groups ? obj.groups.map(group => this._groupForm(group)) : [])
    });
  }

  private _groupForm(obj: GroupModel = {}): FormGroup {
    return this.fb.group({
      id: obj.id || null,
      name: obj.name || null,
      groupOfGroup: obj.groupOfGroup || null,
      authorities: this.fb.array(obj.authorities ? obj.authorities.map(authority => this._authorityForm(authority)) : [])
    });
  }

  private _authorityForm(obj: AuthorityModel = {}): FormGroup {
    return this.fb.group({
      type: obj.type || null,
      subTypes: this.fb.array(obj.subTypes ? obj.subTypes.map(subType => this._subTypeForm(subType)) : [])
    });
  }

  private _subTypeForm(obj: SubTypeModel = {}): FormGroup {
    return this.fb.group({
      subType: obj.subType || null,
      roles: this.fb.array(obj.roles ? obj.roles.map(role => this._roleForm(role)) : [])
    });
  }

  private _roleForm(obj: RoleModel = {}): FormGroup {
    return this.fb.group({
      name: obj.name || null,
      description: obj.description || null
    });
  }

}
