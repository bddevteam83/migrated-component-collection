export interface SearchAutocompleteConfig {
  placeholder: string;
  initValue?: any;
  displayPaths?: string[];
  disabled?: boolean;
}
