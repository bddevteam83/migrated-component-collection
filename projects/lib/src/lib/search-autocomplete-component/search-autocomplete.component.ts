import {Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output} from '@angular/core';
import {SearchAutocompleteConfig} from './search-autocomplete.config';

import * as _ from 'lodash';
import {FormControl} from '@angular/forms';
import {Subject} from 'rxjs';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';

@Component({
  selector: 'bd-search-autocomplete',
  templateUrl: './search-autocomplete.component.html',
  styleUrls: ['./search-autocomplete.component.scss']
})
export class SearchAutocompleteComponent implements OnChanges, OnInit, OnDestroy {

  @Input() data: any[] = [];
  @Input() config: SearchAutocompleteConfig;

  control: FormControl = new FormControl(null);

  @Output() searchChanged: EventEmitter<string> = new EventEmitter();
  @Output() optionSelected: EventEmitter<any> = new EventEmitter();
  private _selectionDone: boolean;
  private _selectedOption: any;

  private _onDestroy: Subject<void> = new Subject();

  constructor() {
  }

  ngOnChanges(): void {
    if (this.config.disabled) {
      this.control.disable();
    } else {
      this.control.enable();
    }
  }

  ngOnInit(): void {
    this.control.setValue(this.config.displayPaths ? _.at(this.config.initValue, this.config.displayPaths).join(' ') : this.config.initValue);

    this.control.valueChanges
      .pipe(distinctUntilChanged(),
        debounceTime(1000))
      .subscribe(value => {
        if (value && !this._selectionDone) {
          this._selectionDone = false;
          this.searchChanged.emit(value);
        }
      });
  }

  ngOnDestroy(): void {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  onOptionSelected(value: any) {
    this._selectedOption = value;
    this._selectionDone = true;

    if (this.config.displayPaths) {
      this.control.setValue(_.at(this._selectedOption, this.config.displayPaths).join(' '));
    } else {
      this.control.setValue(this._selectedOption);
    }

    this.optionSelected.emit(this._selectedOption);
  }
}
