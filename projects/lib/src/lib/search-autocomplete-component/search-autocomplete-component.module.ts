import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SearchAutocompleteComponent} from './search-autocomplete.component';
import {MatAutocompleteModule, MatFormFieldModule, MatIconModule, MatInputModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AtObjectPipeModule} from 'bd-pipe-collection';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  declarations: [SearchAutocompleteComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    MatIconModule,

    TranslateModule,

    AtObjectPipeModule,
  ],
  exports: [SearchAutocompleteComponent]
})
export class SearchAutocompleteComponentModule {
}
